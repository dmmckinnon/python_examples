import random
randomNumber = random.randint(1, 10)
guess = int(input("Please enter your guess: "))
guessCount = 1

while guess != randomNumber:
    print("Your guess was not correct")

    if guess > randomNumber:
        print("You guessed to high")
    else:
        print("You guessed to low")

    guess = int(input("Please enter you guess: "))
    guessCount += 1

print("You guessed correctly. It took you ", guessCount, " guesses")
