#I want to use functions
#I want get information from the user - getPersonInfo()
#I want to store the information in a file - writeToFile(aPersonList)
#I want to read the information back from the file - showPersonInfo()
#I want the user to enter as many records as they want - while loopController():

def getPersonInfo():
    firstName = input("Please enter your first name: ")
    lastName = input("Please enter your last name: ")
    age = int(input("Please enter your age: "))
    height = int(input("Please enter your height in cm: "))
    personInfoList = [firstName, lastName, age, height]
    return personInfoList

def writeToFile(aPersonList):
    f = open("PersonInfoFile.txt", 'a')
    for line in aPersonList:
        f.write(str(line) + " ")
    f.write("\n")
    f.close()

def showPersonInfo():
    f = open("PersonInfoFile.txt", 'r')
    displayPersonInfo = f.read()
    print(displayPersonInfo)
    f.close()

def loopController():
    answer = input("Would you like to enter person records? Y for Yes N for No: ")
    if answer.upper() == "Y":
        return True
    else:
        return False


def main():
    while loopController():
        pInfoList = getPersonInfo()
        writeToFile(pInfoList)

    showPersonInfo()
    
    #print("The first name you entered is" , pInfoList)

main()
