def encryptText(data, encryptTo):
    encryptedData = ""
    for x in data:
        encryptedData += chr(ord(x) + encryptTo)
    return encryptedData

def unEncryptText(data, deCryptTo):
    unEncyrptedText = ""
    for a in data:
        unEncyrptedText += chr(ord(a) - deCryptTo)
    return unEncyrptedText

def encryptFile(fileName, encryptTo):
    encryptedData = ""
    f = open(fileName, 'r')
    data = f.read()
    f.close()
    encryptedData = encryptText(data, encryptTo)
    f = open("EncryptedFile.txt", 'w')
    encryptedData += chr(encryptTo)
    f.write(encryptedData)
    f.close()

def unEncryptFile(fileName):
    unEncyrptedText = ""
    f = open(fileName, 'r')
    data = f.read()
    deCryptTo = ord(data[-1])
    data = data[0:-1]
    f.close()
    for a in data:
        unEncyrptedText += chr(ord(a) - int(deCryptTo))
    f = open("Decrypted.txt", 'w')
    f.write(unEncyrptedText)
    f.close()

def levelCheck():
    level = int(input("Please enter the level of encryption 1 - 5: "))
    while level not in range(1,5):
        print("Level must be between 1 and 5.")
        level = int(input("Please enter the level of encryption 1 - 5: "))
    return level
