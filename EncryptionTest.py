import EncryptionLibrary
def main():
    data = input("Please enter the data to be encrypted: ")
    level = EncryptionLibrary.levelCheck()
    encryptedData = EncryptionLibrary.encryptText(data, level)
    unEncyrptedText = EncryptionLibrary.unEncryptText(encryptedData, level)

    print("The encypted text is", encryptedData)

    print("The unencrypted text is", unEncyrptedText)

main()
