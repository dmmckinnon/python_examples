import EncryptionLibrary

def main():
    fileName = input("Please enter the file name to encrypt right now: ")
    level = EncryptionLibrary.levelCheck()
    EncryptionLibrary.encryptFile(fileName, level)

main()
