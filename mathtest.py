"""
Donnie McKinnon
Feb 15 2019
CPRG-1000
Assignment 1
Exercise 9
"""
import math

print("Donnie McKinnon")

name = input("Please enter your name: ")
print(name)

age = int(input("Please enter your age: "))
print("Your name is " + name + " and your age is " + str(age))
print("Your name is", name, "and your age is", age)

birthYear = 2019 - age

print("The year you were born is", birthYear)

num1 = int(input("Please enter first number: "))
num2 = int(input("Please enter the second number: "))

addition = num1 + num2
subtraction = num1 - num2
multiply = num1 * num2
divide = num1 / num2

modules = num1 % num2

print("The sum is", addition)
print("The difference is", subtraction)
print("Multiplied is", multiply)
print("Divided is", divide)
print("Modulus is", modules)



print("Pi to two decimal places is:",round(math.pi,2))
decimal = int(input("Please enter the decimal places to round pi to: "))
print("Pi rounded to", decimal, "is", round(math.pi, decimal))
