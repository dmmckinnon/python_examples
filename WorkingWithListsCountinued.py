numList = [1,2,3,4,5]
strList = ["Monday", "Tuesday", "Wednesday"]

numList.append(6)
print(numList)

strList.append("Thursday")
print(strList)

strList.append("Friday")
print(strList)

#strList.append("Saturday", "Sunday") Can't do this

strList.extend("67")
print(strList)

strList.insert(2, "Sunday")
print(strList)

print(numList.pop(2))
print(numList)
