count = 0

while count < 100:
    print("The current value of count is:", count)
    count += 5

for count in range(0, 100, 5):
    print("The current value of count is:", count)
