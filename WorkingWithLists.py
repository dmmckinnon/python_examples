numList = [1,2,3,4,5]
strList = ["Monday", "Tuesday", "Wednesday"]

numListLength = len(numList)
strListLength = len(strList)

print("The length of the num list is", numListLength)
print("The length of the str list is", strListLength)

print("The numlist and the strlist joined together is", numList + strList)

if numList == strList:
    print("The two are equal")
else:
    print("The two are not equal")

print("This is slice of the numList", numList[1:3])

longerNumList = numList + [6,7,8]
print("This is a longer numb list", longerNumList)

reallyLongNumList = longerNumList + list(range(9, 101))
print("This is a really long list of numbers", reallyLongNumList)

if 77 in reallyLongNumList:
    print("True")
else:
    print("False")

print("This is the first element in the string list", strList[0])

lastElementStrList = len(strList) - 1
print("This is the last element in the string list", strList[lastElementStrList])

print("This might be easier to get the last element in a list", strList[-1])

for element in strList:
    print(element)

numList[2] = 55
print(numList)

strList[1] = "Friday"
print(strList)

