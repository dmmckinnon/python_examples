def fullName(fName, lName):
    return fName + " " + lName

def main():
    firstName = "Jim"
    lastName = "McKenna"
    print(fullName(firstName, lastName))

main()
