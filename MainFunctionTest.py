def fullName(firstName, lastName):
    """Returns frist name and last name combined into full name"""
    return firstName + " " + lastName

def printName(firstName, lastName):
    print(firstName, lastName)

def main():
    fName = "Donnie"
    lName = "McKinnon"
    print("Hello your name is", fullName(fName, lName))
    printName(fName, lName)

main()
