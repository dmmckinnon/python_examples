import random

def loopController(total, playCount):
    if playCount > 2:
        if total > 21:
            print("Busted")
            return False
        else:
            loopControl = input("Hit ? ")
            if loopControl.upper() == 'Y':
                return True
            else:
                print("Player holds at ", total)
                return False
    else:
        return True
    
def getCardValue(number):
    cardValue = 0
    if number == 14:
        cardValue = 11
    elif number in range(11, 14):
        cardValue = 10
    else:
        cardValue = number
    return cardValue

def getCardDesc(number):
    if number == 14:
        return 'A'
    elif number == 13:
        return 'K'
    elif number == 12:
        return 'Q'
    elif number == 11:
        return 'J'
    else:
        return number

def main():
    spadeList = [] # List 1
    heartList = [] # List 2
    clubsList = [] # List 3
    diamondList = [] # List4
    playerList = [] # hold the players cards
    houseList = [] # hold the houses cards
    playerTotal = 0
    houseTotal = 0
    playCount = 1
    play = input("Would you like to play again? Y = Yes, N = No: ")
    while play.upper() == 'Y':
        while loopController(playerTotal, playCount):
            cardAlreadyPlayed = True
            playCount += 1
            while cardAlreadyPlayed:
                randNumber = random.randint(1, 14)
                randSuit = random.randint(1, 4)
                if randSuit == 1: # spadeList
                    if randNumber in spadeList:
                        cardAlreadyPlayed = True
                    else:
                        spadeList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                elif randSuit == 2: #heartList
                    if randNumber in heartList:
                        cardAlreadyPlayed = True
                    else:
                        heartList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                elif randSuit == 3: #clubsList
                    if randNumber in clubsList:
                        cardAlreadyPlayed = True
                    else:
                        clubsList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                else: # diamondList
                    if randNumber in diamondList:
                        cardAlreadyPlayed = True
                    else:
                        diamondList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                if cardAlreadyPlayed == False:
                    playerTotal += getCardValue(randNumber)
                    playerList.append(getCardDesc(randNumber))
                    print("Players cards", playerList)
                    print("Players total",playerTotal)

        if playerTotal < 22: # no need to calculate house if player is busted
            while houseTotal <= playerTotal:
                randNumber = random.randint(1, 13)
                randSuit = random.randint(1, 4)
                if randSuit == 1: # spadeList
                    if randNumber in spadeList:
                        cardAlreadyPlayed = True
                    else:
                        spadeList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                elif randSuit == 2: #heartList
                    if randNumber in heartList:
                        cardAlreadyPlayed = True
                    else:
                        heartList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                elif randSuit == 3: #clubsList
                    if randNumber in clubsList:
                        cardAlreadyPlayed = True
                    else:
                        clubsList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                else: # diamondList
                    if randNumber in diamondList:
                        cardAlreadyPlayed = True
                    else:
                        diamondList.append(randNumber)
                        cardAlreadyPlayed = False
                        
                if cardAlreadyPlayed == False:
                    houseTotal += getCardValue(randNumber)
                    houseList.append(getCardDesc(randNumber))
                    print("House cards", houseList)
                    print("House total",houseTotal)
            print("Player total", playerTotal)
            print("House total", houseTotal)
            if houseTotal > 21:
                print("The player wins")
            else:
                print("The house wins")
        
        else:
            print("Player busts house wins")

        playerList = [] 
        houseList = [] 
        playerTotal = 0
        houseTotal = 0
        playCount = 1
        play = input("Would you like to play again? Y = Yes, N = No: ")
    """
    print("Spadelist", spadeList)
    print("Heartlist", heartList)
    print("Clubslist", clubsList)
    print("Diamondlist", diamondList)
    """

main()
        
