def square(x):
    return x * x

def main():
    result = square(4)
    print("Four squared is", result)

main()
