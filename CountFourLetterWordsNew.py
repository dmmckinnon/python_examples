f = open("myfile.txt", 'r')
fourLetterWordCount = 0
for line in f:
    wordlist = line.split()
    for word in wordlist:
        if len(word) == 4:
            fourLetterWordCount += 1
print("There are", fourLetterWordCount, "four letter words in the file")
