def square(x):
    return x * x

def main():
    squared = square(7)
    print("The value of 7 squared is", squared)

main()
