f = open("newfile.txt", 'r')
inputFile = open("fourLetterWordFile.txt", 'w')

lineCount = 0 #count the number of lines
wordCount = 0 #count the totel number of words
fourLetterWordCount = 0
for line in f: #reading each line of the file
    #print(line)
    lineCount += 1
    wordList = line.split()
    print(wordList)
    wordsPerList = 0 #count the words per list
    for word in wordList: #loop through wordlist
        wordsPerList += 1
        if len(word.strip(".")) == 4: #striping the '.' from words 
            fourLetterWordCount += 1
            inputFile.write(word.strip(".") + ',')
    print("There were", wordsPerList, "words in this list")
    wordCount += wordsPerList

inputFile.close()
print("There are", wordCount, "words in the file.")
print("There are", lineCount, "lines in the file.")
print("There are", fourLetterWordCount, "four letter words in the file")
