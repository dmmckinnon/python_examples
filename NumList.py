def askUser():
    return input("Please enter y to continue")
def addNum(nums):
    return nums[0] + nums[1]
def subNum(nums):
    return nums[0] - nums[1]
def multiNum(nums):
    return nums[0] * nums[1]
def divNum(nums):
    return nums[0] // nums[1]
def main():
    loopCtrVar = askUser()
    while loopCtrVar == 'y':
        num1 = int(input("num one "))
        num2 = int(input("num two "))
        numbers = [num1, num2]
        decision = int(input("Please enter 1 - 4"))
        if decision == 1:
            print(addNum(numbers))
        elif decision == 2:
            print(subNum(numbers))
        elif decision == 3:
            print(multiNum(numbers))
        elif decision == 4:
            print(divNum(numbers))
        else:
            print("you entered incorrectly")
        loopCtrVar = askUser()
main()
